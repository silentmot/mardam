<?php

namespace Afaqy\Integration\Actions\Aggregators;

use Afaqy\Core\Actions\Aggregator;
use Afaqy\Integration\Actions\SlfCalls\SendCarInformationToSLFAction;

class HandleZkCarInformationAggregator extends Aggregator
{
    /**
     * @var \Afaqy\Integration\DTO\ZkCarInfoData
     */
    public $data;

    /**
     * @param \Afaqy\Integration\DTO\ZkCarInfoData $data
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function execute()
    {
		\Log::debug('SendCarInformationToSLFAction start');
        $car_info = (new SendCarInformationToSLFAction($this->data))->execute();

        if (!$car_info) {
            return false;
        }
		\Log::debug('SendCarInformationToSLFAction end');

		\Log::debug('HandleEntrancePermissionAggregator start');
        if ($car_info['operation_type'] == 'entrancePermission') {
            return (new HandleEntrancePermissionAggregator($car_info))->execute();
        }
		\Log::debug('HandleEntrancePermissionAggregator end');

		\Log::debug('HandleStartTripAggregator start');
        if ($car_info['operation_type'] == 'startTrip') {
            return (new HandleStartTripAggregator($car_info))->execute();
        }
		\Log::debug('HandleStartTripAggregator end');

		\Log::debug('HandleScaleAggregator start');
        if ($car_info['operation_type'] == 'scale') {
            return (new HandleScaleAggregator($car_info))->execute();
        }
		\Log::debug('HandleScaleAggregator end');

        return false;
    }
}
