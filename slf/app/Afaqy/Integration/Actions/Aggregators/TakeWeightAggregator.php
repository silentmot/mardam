<?php

namespace Afaqy\Integration\Actions\Aggregators;

use Afaqy\Core\Actions\Aggregator;
use Afaqy\Integration\Actions\DevicesCalls\OpenGateAction;
use Afaqy\Integration\Actions\DevicesCalls\TakeWeightAction;
use Afaqy\Integration\Actions\SlfCalls\SendWeightToSLFAction;

class TakeWeightAggregator extends Aggregator
{
    /**
     * @var array
     */
    public $data;

    /**
     * @param array $data
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function execute()
    {
		\Log::debug('TakeWeightAction start');
        $data = (new TakeWeightAction($this->data))->execute();

        if (!$data) {
            return false;
        }
		\Log::debug('TakeWeightAction end');

		\Log::debug('SendWeightToSLFAction start');
        $response = (new SendWeightToSLFAction($data))->execute();

        if (!$response) {
            return false;
        }
		\Log::debug('SendWeightToSLFAction end');

		\Log::debug('OpenGateAction start');
        return (new OpenGateAction($data['devices'], $data['unit']['rfid']))->execute();
    }
}
